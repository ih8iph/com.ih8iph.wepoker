/* 
 * RTC AG, Copyright (c) 2014 
 * created 17.02.2014 by boschund 
 */ 
package com.bod;

public class PException extends Exception
{
    private static final long serialVersionUID = 7851762128983937099L;
    String reason = "";
    String todo = "";
    
    PException(String reason, String todo, Throwable e)
    {
        super(e==null?new Throwable():e);
        this.reason = reason;
        this.todo = todo;
    }
    
    public String toString()
    {
        StringBuilder res = new StringBuilder("Es ist ein Fehler aufgetreten: \n");
        res.append("Grund : " + reason);
        res.append("\n");
        res.append("ToDo  : " + todo);
        res.append("\n");
        res.append("*****************************************\n");
        return res.toString();
    }
}


