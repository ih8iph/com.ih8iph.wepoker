/* 
 * RTC AG, Copyright (c) 2014 
 * created 13.02.2014 by boschund 
 */
package com.bod;

public enum Color {
    HERZ(1), ECKEN(2), KREUZ(3), SCHAUFEL(4);

    /**
     *  The Rank value. 
     */
    private int value;

    Color(int value)
    {
        this.value = value;
    }

    public int value()
    {
        return value;

    }
    
}
