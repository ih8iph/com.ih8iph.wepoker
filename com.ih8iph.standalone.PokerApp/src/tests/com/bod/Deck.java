package com.bod;

import java.util.ArrayList;
import java.util.Random;

/* 
 * RTC AG, Copyright (c) 2014 
 * created 13.02.2014 by boschund 
 */

public class Deck
{
    private ArrayList<Card> cards;

    Deck()
    {
        cards = new ArrayList<Card>();
        int index_1, index_2;
        Random g1, g2, g3, g4;
        Card temp;

        for (short a = 0; a <= 3; a++)
        {
            for (short b = 0; b <= 12; b++)
            {
                cards.add(new Card(Color.values()[a], Rank.values()[b]));
            }
        }

        int size = cards.size() - 1;

        // mische
        g1 = new Random();
        g2 = new Random();
        for (short i = 0; i < 100; i++)
        {
            index_1 = g1.nextInt(size) + 1;
            index_2 = g2.nextInt(size) + 1;

            temp = (Card) cards.get(index_2);
            cards.set(index_2, cards.get(index_1));
            cards.set(index_1, temp);
        }
        // mische nochmals
        g3 = new Random();
        g4 = new Random();
        for (short i = 0; i < 100; i++)
        {
            index_1 = g3.nextInt(size);
            index_2 = g4.nextInt(size);

            temp = (Card) cards.get(index_2);
            cards.set(index_2, cards.get(index_1));
            cards.set(index_1, temp);
        }
    }

    public Card drawFromDeck()
    {
        return cards.remove(cards.size() - 1);
    }

    public int getTotalCards()
    {
        return cards.size();
    }

}
