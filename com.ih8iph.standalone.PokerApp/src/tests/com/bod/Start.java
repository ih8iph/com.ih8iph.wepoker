package com.bod;

/* 
 * RTC AG, Copyright (c) 2014 
 * created 13.02.2014 by boschund 
 */

public class Start
{
    public static void main(String[] args)
    {
        Deck deck = new Deck();
        Hand hand1 = new Hand(deck);
        Hand hand2 = new Hand(deck);
        Hand hand3 = new Hand(deck);
        Hand hand4 = new Hand(deck);
        Hand hand5 = new Hand(deck);
        
        hand1.displayHand();
        hand2.displayHand();
        hand3.displayHand();
        hand4.displayHand();
        hand5.displayHand();
        
        HandEvaluator he = new HandEvaluator(hand1);
        
    }

}
