/* 
 * RTC AG, Copyright (c) 2014 
 * created 13.02.2014 by boschund 
 */ 
package com.bod;

public class Hand {
    private Card[] cards;
    private int[] value;

    Hand(Deck d)
    {
        value = new int[3];
        cards = new Card[2];
        for (int x=0; x<2; x++)
        {
            cards[x] = d.drawFromDeck();
        }
    }
   
    void displayHand()
    {
        System.out.println("Hand:");
        for (int x=0; x<2; x++)
            System.out.print(" " + cards[x]);
        System.out.println();
    }

    int compareTo(Hand that)
    {
        for (int x=0; x<3; x++)
        {
            if (this.value[x]>that.value[x])
                return 1;
            else if (this.value[x]<that.value[x])
                return -1;
        }
        return 0; //if hands are equal
    }
    
    Card[] getCards()
    {
        return cards;
    }
    
}

