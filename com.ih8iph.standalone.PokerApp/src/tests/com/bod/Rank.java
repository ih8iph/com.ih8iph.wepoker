/* 
 * RTC AG, Copyright (c) 2014 
 * created 13.02.2014 by boschund 
 */
package com.bod;

public enum Rank {
    _A(1), _2(2), _3(3), _4(4), _5(5), _6(6), _7(7), _8(8), _9(9), _T(10), _J(11), _Q(12), _K(13);

    /**
     *  The Rank value. 
     */
    private int value;

    Rank(int value)
    {
        this.value = value;
    }

    public int value()
    {
        return value;
    }

}
