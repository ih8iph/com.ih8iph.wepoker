/* 
 * RTC AG, Copyright (c) 2014 
 * created 13.02.2014 by boschund 
 */
package com.bod;


public class Card implements Comparable<Card>
{
    private Rank rank;

    private Color color;

    Card(Color suit, Rank rank)
    {
        this.rank = rank;
        this.color = suit;
    }

    public @Override
    String toString()
    {
        return color.toString() + rank.toString();
    }

    public Rank getRank()
    {
        return rank;
    }

    public Color getColor()
    {
        return color;
    }

    @Override
    public int compareTo(Card o)
    {
        // TODO Auto-generated method stub
        return 0;
    }

}
